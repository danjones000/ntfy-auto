<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\SignatureValidationException as Exc;
use App\Support\Facades\Ntfy;
use Illuminate\Support\Arr;

class SignatureValidator
{
    public function validate(): void
    {
        $key = env('NTFY_VALIDATION_KEY');
        if (empty($key)) {
            throw new Exc('Missing NTFY validation key');
        }

        $actions = Ntfy::actions();
        $sig = Arr::first($actions, fn (array $action) => $action['action'] === 'broadcast' && ($action['intent'] ?? null) === 'com.danielrayjones.ntfy-auto.SIGNATURE');
        $sig = Arr::get($sig, 'extras.signature');

        if (empty($sig)) {
            throw new Exc('No signature found in request');
        }

        $orig = json_decode(Ntfy::raw(), true);
        $text = ($orig['title'] ?? '') . $orig['message'];
        $expected = base64_encode(hash_hmac('sha1', $text, $key, true));

        if ($sig !== $expected) {
            throw new Exc('Signature does not match');
        }
    }
}
