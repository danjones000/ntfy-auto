<?php

declare(strict_types=1);

namespace App\Services;

use App\Runners\Runner;
use App\Support\Facades\Ntfy;
use Illuminate\Support\Str;

class RunnerFactory
{
    /**
     * Runner must be explicitly enabled in environment with a title corresponding to a runner.
     *
     * Use NTFY_RUNNER_THE_TITLE=RunnerClass
     * RunnerClass is the class without the namespace.
     */
    public function getRunner(): ?Runner
    {
        $title = Str::of(Ntfy::title())->studly()->snake()->upper()->__toString();
        $class = env('NTFY_RUNNER_' . $title);
        if (empty($class)) {
            return null;
        }

        $class = 'App\\Runners\\' . $class;
        if (!class_exists($class)) {
            return null;
        }

        $interfaces = class_implements($class);
        if (!in_array(Runner::class, $interfaces)) {
            return null;
        }

        return app($class);
    }
}
