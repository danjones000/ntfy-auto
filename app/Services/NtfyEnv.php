<?php

declare(strict_types=1);

namespace App\Services;

use DomainException;
use Illuminate\Support\Carbon;

class NtfyEnv
{
    protected string $raw;
    protected array $data;
    protected string $id;
    protected string $topic;
    protected Carbon $time;
    protected string|array $message;
    protected int $priority;

    public function __construct()
    {
        $this->raw = env('NTFY_RAW') ?? '';
    }

    public function raw(): string
    {
        return $this->raw;
    }

    public function data(): array
    {
        return $this->data ??= (function () {
            $data = json_decode($this->raw, true);
            if (json_last_error() !== JSON_ERROR_NONE || !is_array($data)) {
                throw new DomainException('NTFY_RAW is not a valid JSON object');
            }

            $message = json_decode($data['message'] ?? '', true);
            if (json_last_error() === JSON_ERROR_NONE && is_array($message)) {
                $data['message'] = $message;
            }

            return $data;
        })();
    }

    public function id(): string
    {
        return $this->id ??= $this->data()['id'];
    }

    public function title(): ?string
    {
        return $this->title ??= $this->data()['title'] ?? null;
    }

    public function time(): Carbon
    {
        return $this->time ??= new Carbon($this->data()['time']);
    }

    public function topic(): string
    {
        return $this->topic ??= $this->data()['topic'];
    }

    public function message(): string|array
    {
        return $this->message ??= $this->data()['message'];
    }

    public function priority(): int
    {
        return $this->priority ??= $this->data()['priority'] ?? 0;
    }

    public function tags(): array
    {
        return $this->tags ??= $this->data()['tags'] ?? [];
    }

    public function actions(): array
    {
        return $this->actions ??= $this->data()['actions'] ?? [];
    }
}
