<?php

namespace App\Support\Facades;

use App\Services\NtfyEnv;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Facade;

/**
 * @method static string raw()
 * @method static array data()
 * @method static string id()
 * @method static Carbon time()
 * @method static ?string title()
 * @method static string|array message()
 * @method static int priority()
 * @method static string[] tags()
 * @method static array actions()
 */
class Ntfy extends Facade
{
    protected static function getFacadeAccessor()
    {
        return NtfyEnv::class;
    }
}
