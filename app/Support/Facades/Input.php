<?php

namespace App\Support\Facades;

use Symfony\Component\Console\Input\InputInterface;
use Illuminate\Support\Facades\Facade;

class Input extends Facade
{
    protected static function getFacadeAccessor()
    {
        return InputInterface::class;
    }
}
