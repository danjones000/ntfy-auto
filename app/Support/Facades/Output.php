<?php

namespace App\Support\Facades;

use Symfony\Component\Console\Output\OutputInterface;
use Illuminate\Support\Facades\Facade;
use Illuminate\Console\Concerns\InteractsWithIO as IO;
use Illuminate\Console\OutputStyle;

class Output extends Facade
{
    protected static function getFacadeAccessor()
    {
        return OutputInterface::class;
    }

    public static function __callStatic($method, $args)
    {
        $ioMethods = get_class_methods(IO::class);
        if (in_array($method, $ioMethods)) {
            $out = static::getFacadeRoot();
            $in = Input::getFacadeRoot();

            if ($out instanceof OutputStyle) {
                $instance = new class {
                    use IO;
                };
                $instance->setOutput($out);
                $instance->setInput($in);

                return $instance->$method(...$args);
            }
        }

        return parent::__callStatic($method, $args);
    }
}
