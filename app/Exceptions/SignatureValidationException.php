<?php

declare(strict_types=1);

namespace App\Exceptions;

class SignatureValidationException extends \RuntimeException {}
