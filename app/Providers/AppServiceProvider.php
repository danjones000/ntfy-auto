<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\NtfyEnv;
use App\Services\RunnerFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NtfyEnv::class);
        $this->app->singleton(RunnerFactory::class);
    }
}
