<?php

namespace App\Runners;

use App\Support\Facades\Ntfy;
use App\Support\Facades\Output;
use Illuminate\Support\Arr;
use Symfony\Component\Process\Process;

class DropLog implements Runner
{
    public function __invoke(): void
    {
        $message = Ntfy::message();
        if (!is_array($message)) {
            return;
        }

        $title = Arr::pull($message, 'title');
        $log = Arr::pull($message, 'log');
        $date = Arr::pull($message, 'date');
        if (!$title || !$log) {
            return;
        }

        $process = ['drop-a-log', $log, $title];
        if ($date) {
            array_push($process, '-d', $date);
        }

        if ($message) {
            array_push($process, '-j', json_encode($message));
        }

        $process = new Process($process);
        $process->run(fn ($type, $buffer) => Output::write($buffer));
        Output::info('done');
    }
}
