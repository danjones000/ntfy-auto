<?php

namespace App\Runners;

interface Runner
{
    public function __invoke(): void;
}
