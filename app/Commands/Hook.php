<?php

namespace App\Commands;

use App\Services\RunnerFactory;
use App\Services\SignatureValidator;
use App\Support\Facades\Ntfy;
use App\Support\Facades\Output;
use App\Support\Facades\Input;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class Hook extends Command
{
    protected $signature = 'hook';
    protected $description = 'Parse NTFY environment variables and do something';

    public function handle(RunnerFactory $runners, SignatureValidator $validator): int
    {
        Output::swap($this->output);
        Input::swap($this->input);

        $this->line(Ntfy::raw());

        $validator->validate();

        $runner = $runners->getRunner();
        if ($runner) {
            $runner();
        } else {
            $this->error('No runner found for ' . Ntfy::title());
        }

        return static::SUCCESS;
    }
}
