<?php

use App\Services\NtfyEnv;
use Illuminate\Support\Carbon;
use function Pest\Faker\faker;

dataset('data', function () {
    $base = [
        'id' => uniqid(),
        'topic' => faker()->word(),
        'message' => faker()->sentence(),
        'time' => time(),
    ];

    yield 'minimal' => [$base];
    yield 'with title' => [$base + ['title' => faker()->sentence()]];
    yield 'with priority' => [$base + ['priority' => faker()->numberBetween(1, 5)]];
    yield 'with tags' => [$base + ['tags' => faker()->words()]];
    yield 'with actions' => [
        $base +
        ['actions' => [[
            'id' => uniqid(),
            'action' => 'broadcast',
            'label' => faker()->word(),
            'clear' => false,
        ]]]
    ];
});

test('empty raw when unset', function () {
    $service = new NtfyEnv();
    expect($service->raw())->toBe('');
});

test('returns raw', function (string $raw) {
    $service = loadNtfy($raw);
    expect($service->raw())->toBe($raw);
})->with([
    'foobar',
    json_encode(['id' => uniqid()]),
]);

test('bad json throws exception', function () {
    $service = loadNtfy('foo bar baz');
    expect(fn () => $service->data())->toThrow(DomainException::class);
});

test('data parsed', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->data())->toBe($data);
})->with('data');

test('get id', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->id())->toBe($data['id']);
})->with('data');

test('get title', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->title())->toBe($data['title'] ?? null);
})->with('data');

test('get topic', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->topic())->toBe($data['topic']);
})->with('data');

test('get time', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->time())->toEqual(new Carbon($data['time']));
})->with('data');

test('get message', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->message())->toBe($data['message']);
})->with('data');

test('get json message', function () {
    $message = ['foo' => 'bar'];
    $data = [
        'id' => uniqid(),
        'topic' => faker()->word(),
        'message' => json_encode($message),
        'time' => time(),
    ];

    $service = loadNtfy(json_encode($data));
    expect($service->message())->toEqual($message);
});

test('data with json message', function () {
    $message = ['foo' => 'bar'];
    $data = [
        'id' => uniqid(),
        'topic' => faker()->word(),
        'message' => json_encode($message),
        'time' => time(),
    ];
    $expected = $data;
    $expected['message'] = $message;

    $service = loadNtfy(json_encode($data));
    expect($service->data())->toEqual($expected);
});

test('get prio', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->priority())->toBe($data['priority'] ?? 0);
})->with('data');

test('get tags', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->tags())->toEqual($data['tags'] ?? []);
})->with('data');

test('get actions', function (array $data) {
    $service = loadNtfy(json_encode($data));
    expect($service->actions())->toEqual($data['actions'] ?? []);
})->with('data');
